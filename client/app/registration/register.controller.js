// Always use an IIFE, i.e., (function() {})();
(function () {
    angular
        .module("DMS")          // to call an angular module, omit the second argument ([]) from the angular.module()
        // syntax this syntax is called the getter syntax
        .controller("RegCtrl", RegCtrl);    // angular.controller() attaches a controller to the angular module
                                            // specified as you can see, angular methods are chainable

    // Dependency injection. An empty [] means RegCtrl does not have dependencies. Here we inject GrocService so
    // RegCtrl can call services related to grocery.
    RegCtrl.$inject = ['$filter', '$state', 'GrocService', 'EmpService'];


    // RegCtrl function declaration. A function declaration uses the syntax: functionName([arg [, arg [...]]]){ ... }

    function RegCtrl($filter, $state, GrocService, EmpService) {

        // Declares the var vm (for ViewModel) and assigns it the object this (in this case, the RegCtrl)
        // Any function or variable that you attach to vm will be exposed to callers of RegCtrl, e.g., register.html
        var vm = this;


        // EXPOSED DATA MODELS ------
        vm.grocery = {
            id: ""
            , name: ""
            , manager: ""
        };
        // Creates a status object
        vm.status = {
            message: ""
            , code: ""
        };

        // EXPOSED FUNCTIONS --------

        vm.register = register;


        // initBX ----------

        initManagerBox();


        // function & delclare and define

        function initManagerBox() {
            EmpService
                .retrieveNonManagers()
                .then(function (results) {
                    console.log("--- EMPLOYEES ----");
                    console.log(results.data);
                    // results.data is expected to contain employee number, first name, and last name
                    vm.employees = results.data;
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });
        }

        //  grocery registration and persistence (i.e., saving to DB)
        function register() {
            // show box displays registration information
            alert("The registration information you sent are \n" + JSON.stringify(vm.grocery));

            // print onto the client console
            console.log("The registration information you sent were:");
            console.log("grocery id: " + vm.grocery.id);
            console.log("grocery name: " + vm.grocery.name);
            console.log("grocery name: " + vm.grocery.manager);

            // Start and end dates
            vm.grocery.from_date = $filter('date')(new Date(), 'yyyy-MM-dd');
            vm.grocery.to_date = new Date('9999-01-01');
            console.log("vm.dept" + JSON.stringify(vm.grocery));

            // We call GrocService.insertDept to handle registration of grocery information. The data shoot out        
            GrocService
                .insertDept(vm.grocery)
                .then(function (result) {
                    console.log("result " + JSON.stringify(result));
                    $state.go("thanks");
                })
                .catch(function (err) {
                    console.log("error " + JSON.stringify(err));
                    vm.status.message = err.data.name;
                    vm.status.code = err.data.parent.errno;
                });
        } // END function register()
    } // END RegCtrl

})();
