
module.exports = function (sequelize, Sequelize) {
    var Grocerylist = sequelize.define("grocery_list",
        {
            id: {
                type: Sequelize.STRING,
                primaryKey: true,
                allowNull: false
            },
            upc12: {
                type: Sequelize.STRING,
                allowNull: false
            },
            brand: {
                type: Sequelize.STRING,
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false,
            freezeTableName: true,
         });

    return Grocerylist;
};
